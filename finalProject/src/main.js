import './assets/main.css'
import * as bootstrap from 'bootstrap'
import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import { createAuth0 } from '@auth0/auth0-vue';

const app = createApp(App)
app.use(
  createAuth0({
    domain: "dev-wsmx7g22wh25b868.us.auth0.com",
    clientId: "jcWoJDfrxcRZ3SgpgGqXgY8qWgsouoaO",
    authorizationParams: {
      redirect_uri: window.location.origin + "/callback"
    }
  })
);
app.use(createPinia())
app.use(router)

app.mount('#app')
