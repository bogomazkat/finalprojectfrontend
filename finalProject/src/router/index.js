import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import CallbackPage from '../views/CallbackPage.vue'
import NewBranch from '../views/NewBranch.vue'
import NewContract from '../views/NewContract.vue'
import NewAgent from '../views/NewAgent.vue'
import NewWage from '../views/NewWage.vue'

import { authGuard } from '@auth0/auth0-vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      beforeEnter: authGuard
    },
    {
      path: '/callback',
      name: 'callback',
      component: HomeView,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue'),
    },
    {
      path: '/newbranch',
      name: 'branch',
      component: NewBranch,
      beforeEnter: authGuard
    },
    {
      path: '/newcontract',
      name: 'contract',
      component: NewContract,
      beforeEnter: authGuard
    },
    {
      path: '/newagent',
      name: 'agent',
      component: NewAgent,
      beforeEnter: authGuard
    },
    {
      path: '/wage',
      name: 'wage',
      component: NewWage,
      beforeEnter: authGuard
    },
  ]
})

export default router
